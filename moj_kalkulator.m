%It is a test version, not ready to be assesed
%exercises 5,6 work in scilab

%exercise 1
h = 6.62607015E-34  %h = Planck costant
zadanie1 = h/2*pi   %calculating the value
% this gives h*pi/2 instead of h/(2*pi)

%exercise 2        %exp(1) is the base of the natural logarithm
zadanie2 = sind(30/exp(1))  %calculating the value

%exercise 3
hex2dec('00123d3')    %converting hexadecimal number to decimal number
zadanie3 = ans/2.455E23   %calculating the value

%exercise 4
zadanie4 = sqrt(exp(1)- pi) %calculating the value

%exercise 5  I don't know how to do it


%exercise 7
f = hex2dec('aabb')
y = log(6371/1E5)   %6371 is the Earth ray expressed in km
%creating some variables just to make it shorter and cleaner
zadanie7 = atan(exp(1)^(sqrt(7)/2 - y)/f) %calculating the value


%exercise 8
avogadro = 6.02214076E23   %number of molecules in 1 mole of substantion

zadanie8 = (avogadro * 10E-6)/5    %calculating the value

%exercise 9 
%mC = 12u mO = 16u H = 1u ethanol = C2H5OH   ethanol = 46u  %calculating the molar mass of ethanol

C12 = (2*12)/46           %1 molecule of ethanol consists of 2 atoms of Carbon. 1 atom of Carbon "has" 12 units (2 atoms of carbon have 24 units [24 of 46 in ethanol])    
zadanie9 = (C12/100)*1000   %Carbon C-13 occurs approximately 100 times less often than Carbon C-12. I multiply the answer by 1000 to get the result in mils.
 




//SCILAB
//exercise 6
t1=[1999 10 09 11 00 13.535]  //That is my birthdate
t2=clock()  //actual date
zadanie6 = etime(t2(),t1)/(3600 * 24) //I multiply E1 by (3600 * 24) to get the answer in days, 


//exercise 5
format(20)    //estabilishing numbers showing on the screen
x=%pi*1E10   //multiplying Pi by 1E10 to have its 10th number after the decimal point before it 
y = fix(x)   //rounding the number to have an integer
y/10      //dividing the number over 10 to get 1 number after decimal point  
fix(ans)  //rounding the number again to get an integer
ans*10    //multiplying by 10 to get number in the same power as "y"
zadanie5 = y-ans    //calculating the value. The result is Pi's 10th number after the decimal point
